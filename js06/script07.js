function uniqu(str) {
    return str.split("").sort().join("");
}

var arr = ['воз', 'киборг', 'корсет', 'ЗОВ', 'гробик', 'костер', 'сектор'];

function anClear(arr) {
    var store = {};

    for (var i = 0; i < arr.length; i++) {
        var index = uniqu(arr[i]).toLowerCase();

        if (index in store) {
            store[index].push(arr[i]);
        } else {
            store[index] = [arr[i]];
        }
        // console.log("#" + i, store);
    }
    var res = [];
    for(var key in store){
        res.push(store[key][0]);
    }
    return res.join(", ");
}

console.log(anClear(arr));
