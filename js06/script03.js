var arr = [1, 2, 3, 4, 5];
arr.sort(randSort);

function randSort(max, min){
    max = 1000;
    min = -1000;
    return Math.random() * (max - min) + min;
}

console.log(randSort());
console.log(arr); // элементы в случайном порядке, например [3,5,1,2,4]
