// функция removeClass смотрит в объект obj перебирает все ключи
// и удаляет в них значение аргумента cls

var obj = {
  a1: 'open menu boom boom room',
  a2: 'roma menu budd boom zoom'

};

function removeClass(obj, cls) {
    for (var key in obj) {
        if (obj[key]) {
            var keySplit = obj[key].split(" ");
            var temp = "";
            for (var i = 0; i <= keySplit.length-1; i++) {
                if (keySplit[i] != cls) {
                    temp = temp + keySplit[i] + " ";
                }
            }
            obj[key] = temp.slice(0, temp.length-1);
        }
        //console.log(obj[key]);
    }
console.log(obj);
}

removeClass(obj, 'boom'); // obj.className='menu'
removeClass(obj, 'blabla'); // без изменений
