var strings = ['кришна', 'кришна', 'харе', 'харе', 'харе', 'харе', 'кришна', 'кришна', '8-()' ];

function unique(str) {
    var sortArr = [];
    str.sort();
    for (var i = 0; i <= str.length-1; i++) {
        if (str[i] != str[i+1]) {
            sortArr.push(str[i]);
        }
    }
    return sortArr;
}

console.log(unique(strings)); // кришна, харе, 8-()

// alternative variant via Object
//https://learn.javascript.ru/task/array-unique
