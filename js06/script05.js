function isPal(str) {
    if (str.replace(/\s+/g,"").toLowerCase() == str.replace(/\s+/g,"").toLowerCase().split("").reverse().join("")) {
        return true;
    } else {
        return false;
    }
}

console.log(isPal('Anna')); // true
console.log(isPal('А роза упала на лапу Азора')); //true
console.log(isPal('Вася')); //false
console.log(isPal('12321')); //true
console.log(isPal('123212')); //false
