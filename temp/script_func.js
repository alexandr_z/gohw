// просто объект
// var alfa = {
//     firstName : "Alex",
//     secondName : "Dou"
// };
//
// console.log(alfa.firstName);


// функция конструктор
// function User (firstName, secondName) {
//     this.firstName = firstName;
//     this.secondName = secondName;
// }

// var Nika = new User("Lol", "Bololo");
// console.log(Nika);
// console.log(Nika.secondName);

// функция конструктор + Getter + Setter
function User() {

    this.firstName = function (firstName) {
        this.firstName = firstName;
        return this.firstName;
    };

    this.secondName = function (secondName) {
        this.secondName = secondName;
        return this.secondName;
    };
};

var Eva = new User("Lol", "Bololo");

Eva.firstName("lololol");
Eva.secondName("lSecccc");

console.log(Eva);
console.log(Eva.firstName);
console.log(Eva.secondName);
