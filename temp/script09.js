function logDecorator(f, log) {
    return function() {
        console.log(log, arguments);
        return f.apply(this, arguments)
    }
}

function work(){

}

work = logDecorator(work, "workLog");
work(5);
work(5, "yu", 8);
work(null);
