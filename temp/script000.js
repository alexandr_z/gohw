function delay(func, ms) {

    return function(){
        var param = arguments;
        var context = this;

        setTimeout(function(){
            func.apply(context, param);
        }, ms);
        return context;
    }
}


function doSomething(a, b){
    return a + b;
}

var a = delay(doSomething, 5000);
console.log(a(1, 2));
