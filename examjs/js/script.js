$( document ).ready(function() {

    console.log( "document ready to start!" );

    /***************
        variables
    ***************/

    $gunman = $("#man");
    $msg = $(".show_msg");
    $gunmanTime = $(".screen_score_gunman");
    $playerTime = $(".screen_score_you");

    var currentSeconds = 0;
    var maxSeconds = 4;
    var timerIntervalVal;
    var timerIntervalPlayerDead;
    var playerTime;
    var startFireTime;


    $("#startGame").on("click", function(){
        $("#startGame").hide();
        goStart();

        //resets
        $msg.text("");
        $playerTime.text("");
    });

    /***************
        start game
    ***************/

    function goStart(){

        $gunmanTime.text(maxSeconds + " sec");

        $gunman.addClass("default_gunman steps_animation");

        $gunman.on("transitionend", function(){
            $gunman.removeClass("steps_animation");
            waitingPosition();
            timerIntervalVal = setInterval(makeTimer, 1000);
        });

        timerIntervalPlayerDead = setInterval(playerDead, 50);
    };

// тела функций

    function waitingPosition() {
        startFireTime = new Date;
        $gunman.addClass("show_face");
        $msg.show().text("Fire");
        $gunman.on("click", function(){
            gunmanDead();
        })
    };

    function makeTimer() {
        currentSeconds++;
        console.log(currentSeconds);
    };

    function calcPlayerTime() {
        return (+playerTime - +startFireTime) / 1000;
    };

    function gunmanDead() { // time checking
        if (currentSeconds < maxSeconds) {
            $gunman.addClass("dead_gunman");
            setTimeout(function() {
                $gunman.removeClass("default_gunman show_face dead_gunman");
                $("#startGame").show();
            }, 2000);
            $msg.text("You Win");
            playerTime = new Date;
            $playerTime.text(calcPlayerTime() + " sec");

            //resets
            clearInterval(timerIntervalVal);
            clearInterval(timerIntervalPlayerDead);
            currentSeconds = 0;
            $gunman.off();
            }
        };

    function playerDead() { // time checking
        if ( currentSeconds >= maxSeconds ){
            $msg.text("You Lose");
            $gunman.removeClass("default_gunman show_face dead_gunman");
            $("#startGame").show();

            //resets
            clearInterval(timerIntervalVal);
            clearInterval(timerIntervalPlayerDead);
            currentSeconds = 0;
            $gunman.off();
        }
    };

}); // end of ready








/***************************\
temp code >>
\***************************/

// setTimeout(function() {
//     $gunman.removeClass("default_gunman show_face dead_gunman")
// }, 2000);

// $gunman.trigger("endDyingAnimation");


// $(document).on("click", ".show_face", function(){
//      $gunman.removeClass("default_gunman");
// });

// if (getClickTime() < makeCurrentTime()) {
//     console.log("U WIN");
// } else {
//     console.log("U LOSE");
// }


// $(document).on('click', '.show_face', function(){
//     console.log("WinWinWin");
// });
