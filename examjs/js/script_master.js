$( document ).ready(function() {

    console.log( "document ready to start!" );

    /***************
        variables
    ***************/

    $gunman = $("#man");

    var currentSeconds = 0;
    var maxSeconds = 2;
    var timerIntervalVal;
    var timerIntervalPlayerDead;


    $("#startGame").on("click", function(){
        $("#startGame").hide();
        goStart();
    });

    /***************
        start game
    ***************/

    function goStart(){

        $gunman.addClass("default_gunman steps_animation");

        $gunman.on("transitionend", function(){
            $gunman.removeClass("steps_animation");
            waitingPosition();
            timerIntervalVal = setInterval(makeTimer, 1000);
        });

        timerIntervalPlayerDead = setInterval(playerDead, 50);

    };

// тела функций

    function waitingPosition() {
        $gunman.addClass("show_face");
        $gunman.on("click", function(){
            gunmanDead();
        })
    };

    function makeTimer() {
        currentSeconds++;
        console.log(currentSeconds);
    };

    function dyingAnimation() {
        
    }



    function gunmanDead() { // time checking
        if (currentSeconds < maxSeconds) {
            $gunman.removeClass("default_gunman");
            $gunman.addClass("dead_gunman");
            clearInterval(timerIntervalVal);
            clearInterval(timerIntervalPlayerDead);
        }
    };

    function playerDead() { // time checking
        if ( currentSeconds == maxSeconds ){
            $(".player_fail_msg").show();
            clearInterval(timerIntervalVal);
            clearInterval(timerIntervalPlayerDead);
        }
    };






}); // end of ready


// герернуть случайный интервал времени
// узнать время онклика
// если клик попал в интервал то WIN







/***************************\
Experement code >>
\***************************/

// $(document).on("click", ".show_face", function(){
//      $gunman.removeClass("default_gunman");
// });

// if (getClickTime() < makeCurrentTime()) {
//     console.log("U WIN");
// } else {
//     console.log("U LOSE");
// }


// $(document).on('click', '.show_face', function(){
//     console.log("WinWinWin");
// });
