var funcQueue = [func1, func2, func3];

function func1() {
    console.log("Im func1");
};
function func2() {
    console.log("Im func2");
};
function func3() {
    console.log("Im func3");
};

function callSequently(funcQueue) {
    if(funcQueue.length) {
        (funcQueue.shift())(function() {
            callSequently(funcQueue);
        });
    }
}
callSequently(funcQueue);
