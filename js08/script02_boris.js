function Calculator() {
    var operations = {
        '+': function(a, b) {
            return a + b;
        },
        '-': function(a, b) {
            return a - b;
        }
    };

    this.calculate = function(str) {
        var parseArr = str.split(' ');
        var op = parseArr[1];
        var a = parseArr[0];
        var b = parseArr[2];

        if (op in operations) {
            return operations[op](a, b);
        } else {
            return false;
        }
    }

    this.addMethod(name, func) {
        if (name in operations) {
            return false;
        } else {
            operations[name] = func;
        }
    }
    return this;
}
