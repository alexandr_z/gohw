function User(fullName) {
    this.fullName = fullName;
    Object.defineProperty(this, "firstName", {
            get: function(){
                // return this.fullName.split(" ")[0]; // variant №1
                return this.fullName.substring(0, this.fullName.indexOf(" "));
            },
            set: function(valueName){
                this.fullName = valueName + " " + this.lastName;
            }
    });
    Object.defineProperty(this, "lastName", {
            get: function(){
                // return this.fullName.split(" ")[1]; // variant №1
                return this.fullName.substring(this.fullName.length, this.fullName.indexOf(" "));
            },
            set: function(valueName){
                this.fullName = this.firstName + " " + valueName;
            }
    });
}


var vasya = new User("Александр Пушкин");
console.log(vasya.fullName);
console.log("getter firstName " + vasya.firstName);
console.log("getter lastName " + vasya.lastName);

// запись в firstName
vasya.firstName = "Петруха";
console.log("setter firstName " + vasya.fullName); // Петруха Пушкин

// запись в lastName
vasya.lastName = "Толстой";
console.log("setter lastName " + vasya.fullName); // Петруха Толстой
