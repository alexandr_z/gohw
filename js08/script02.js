function Calculator() {

    var operations = {
        "-": function(a, b) {
            return a - b;
        },
        "+": function(a, b) {
            return a + b;
        }
    };

    this.calculate = function(str) {
        var splitStr = str.split(' ');
        var a = +splitStr[0];
        var op = splitStr[1];
        var b = +splitStr[2];

        if (!operations[op] || isNaN(a) || isNaN(b)) {
          return NaN + " (Error - Not valid input)";
        }

        return operations[op](+a, +b);
    }

    this.addMethod = function(name, func) {
        operations[name] = func;
    };
    return this;
}

var calc = new Calculator;

calc.addMethod("*", function(a, b) {
    return a * b;
});
calc.addMethod("/", function(a, b) {
    return a / b;
});
calc.addMethod("**", function(a, b) {
    return Math.pow(a, b);
});

var result = calc.calculate("2 * 60");
console.log(result);; // 12


// var Calculator = function(){
//     this.calculate = function(str) {
//     this.str = str;
//         switch (this.str.split(" ")[1]) {
//             case "+":
//                 return (+this.str.split(" ")[0] + +this.str.split(" ")[2]);
//                 break;
//             case "-":
//                 return (+this.str.split(" ")[0] - +this.str.split(" ")[2]);
//                 break;
//             default:
//                 return "WTF??? =>> " + this.str.split(" ")[1];
//                 break;
//         }
//     },
//     this.addMethod = function(name, func){
//         this.name = name.split(" ")[1];
//         this.func = function(a,b) {
//             return console.log(this.name);
//         }
//     }
// }
//
// var firstCalc = new Calculator;
// console.log(firstCalc.calculate("10 + 4"));
//
// var powerCalc = new Calculator;
// powerCalc.addMethod('*', function(a, b) {
//   return a * b;
// });
//
// var result = powerCalc.addMethod('2 * 3');
// console.log( result ); // 8






// Нужно добавить в конструктор метод addMethod
// который обработает ('*', function(a, b)) как аргумент и отдаст а и b



// this.makeCalculate = function() {
//     if (this.str.split(" ")[1] == "+") { // plus
//         return +this.str.split(" ")[0] + +this.str.split(" ")[2];
//     } if (this.str.split(" ")[1] == "-") { // minus
//         return +this.str.split(" ")[0] - +this.str.split(" ")[2];
//     } if (this.str.split(" ")[1] == "*") { // multiplication
//         return +this.str.split(" ")[0] * +this.str.split(" ")[2];
//     } if (this.str.split(" ")[1] == "/") { // division
//         return +this.str.split(" ")[0] / +this.str.split(" ")[2];
//     }
// }
