// FIRST VARIANT //

// var image = {
//     width: 100,
//     height: 400,
//     title: 'Cool image'
// };
//
// for (var key in image) {
//     if (!isNaN(image[key])) {
//         image[key] *= 2;
//     }
// }
// console.log(image);

// SECOND VARIANT //

var image = {
    width: 100,
    height: 400,
    title: 'Cool image'
};

var boom = {
    width: 500,
    height: 2000,
    title: 'Very Cool image'
};



function isNumeric(n) {
  return !isNaN(parseFloat(n)) && isFinite(n);
}

function multiplyNumeric(arr) {
    for (var key in arr) {
        if (isNumeric(arr[key])) {
            arr[key] *= 2;
        }
    }
    console.log(arr);
}

multiplyNumeric(image);
multiplyNumeric(boom);
