/**
* функция извлекает из объекта obj все числовые данные и
* помещает их в масив, сохраняя порядок
* @pamam Object 0bj
* @return Array
**/

var obj = {
  person1Age: 20,
  person1Name: 'Ivanov',
  person2Age: 30,
  person2Name: 'Petrov',
  person3Age: 40,
  person3Name: 'Sidorov'
};

function extractNumber(obj) {
    var arr = [];
    for (var key in obj) {
        if (isNumeric(obj[key])) {
            arr.push(obj[key]);
        }
    }
    return arr;
}

function isNumeric(num) {
    return (typeof num == 'number');
}

console.log(extractNumber(obj));


/**
* функция извлекает из объекта obj все стринговые данные и
* помещает их в масив, сохраняя порядок
* @pamam Object 0bj
* @return Array
**/

function extractStr(obj) {
    var arr = [];
    for (var key in obj) {
        if (isString(obj[key])) {
            arr.push(obj[key]);
        }
    }
    return arr;
}

function isString(num) {
    return (typeof num == 'string');
}

console.log(extractStr(obj));
