function getMaxNumber(arg) {
    return arg.sort(function(a,b) {
        return b - a;
    })[0];
}

console.log(getMaxNumber([1,30,40,2,7])); // 40
console.log(getMaxNumber([1,15,-20,2,-7])); // 15
