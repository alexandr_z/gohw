/**
 * Считает сколько раз использовалось слово в массиве
 * @param array arr
 * @return Object
 */

function countClassNameInString(arr) {
    var obj = {};
    for (var i = 0; i < arr.length; i++) {
        if (arr[i] in obj) {
            obj[arr[i]] = obj[arr[i]] + 1;
        } else {
            obj[arr[i]] = 1;
        }
    }

    return obj;
}

/**
 * Из массива выбирает уникальные имена классов,
 * сортируя их по частоте использования
 *
 * @param array arr
 * @return Object
 */

function getMostUseClass(arr) {
    var max = 0;
    var objClass = countClassNameInString(arr);

    return Object.keys(objClass).sort(function(a,b){
        return objClass[ b ] - objClass[ a ]
    });
    for (var kei in objClass) {
        if (objClass[key] > max) {
            max = objClass[key];
            className = key;
        }
    }
}

var arr =  ['link', 'menu', 'menu__item', 'menu__item', 'header', 'link', 'footer', 'sidebar', 'link'];

console.log(getMostUseClass(arr));
