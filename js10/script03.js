function makeAlphabeticalSort(str) {
    
    return str.split("").sort().join("");
}

console.log(makeAlphabeticalSort("webmaster"));
