/**
* ф-ция переводит первую букву аргумента в
* верхний регистр и возвращает уже модифицированное слово
**/

function capitalizeFirstLetter(word) {

    return word.charAt(0).toUpperCase() + word.slice(1);
};

/**
* Ф-ция переводит аргумент в массив.
* Через FOR в каждой итерации
* используя capitalizeFirstLetter
* переводит первую букву в верхний регистр
* каждому елементу arr
**/

function makeCapitalize(str) {
    var arr = str.split(" ");
    for (var i = 0; i <= arr.length-1; i++) {
        arr[i] = capitalizeFirstLetter(arr[i]);
    }

    return arr.join(" ");
};

console.log(makeCapitalize("the quick brown fox"));
