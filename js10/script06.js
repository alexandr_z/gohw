function showTypeOfArgument(arg) {
    return typeof arg;
}

console.log(showTypeOfArgument(3543));
console.log(showTypeOfArgument("3543"));
console.log(showTypeOfArgument({f:1, f:2}));
console.log(showTypeOfArgument([45,33]));
console.log(showTypeOfArgument(NaN));
console.log(showTypeOfArgument(true));
console.log(showTypeOfArgument(function function_name(argument) {/* body... */}));
