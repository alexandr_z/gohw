arr = [43, 5, 23, 555, 77, 9];

function shuffle(arr) {
    return arr.sort(function(){
        return Math.random() - 0.5;
    });
}

console.log(shuffle(arr));
